﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegExchLibrary
{
    class APIData
    {
        public string APIKey { get; set; }
        public string Secret { get; set; }
    }
}
