﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SegExchLibrary
{
    public class Client
    {
        private Uri URL;
        private string APIKey;

        public Client(string url, string apikey)
        {
            this.URL = new Uri(url, true);
            this.APIKey = apikey;
        }

        public APIResponse SendSecret(string text)
        {
            APIResponse apiResponse = null;

            try
            {
                HttpWebRequest sendRequest = (HttpWebRequest)System.Net.WebRequest.Create(this.URL);
                sendRequest.ContentType = "application/json; charset=utf-8";
                sendRequest.Method = "POST";

                APIData data = new APIData{ APIKey = this.APIKey, Secret = text };
                string json = JsonConvert.SerializeObject(data, Formatting.None);

                using (StreamWriter streamWriter = new StreamWriter(sendRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                HttpWebResponse response = (HttpWebResponse)sendRequest.GetResponse();
                using (var streamReader = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    string responseText = streamReader.ReadToEnd();
                    apiResponse = (APIResponse)JsonConvert.DeserializeObject<APIResponse>(responseText);
                }
            }
            catch (WebException ex)
            {
                throw new APIException(ex.Message.ToString());
            }
            catch(Exception ex)
            {
                throw new APIException(ex.Message.ToString());
            }

            return apiResponse;
        }

    }
}
