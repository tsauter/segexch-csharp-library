﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegExchLibrary
{
    class APIException : Exception
    {
        public APIException(string message) : base(message)
        {
        }
    }
}
