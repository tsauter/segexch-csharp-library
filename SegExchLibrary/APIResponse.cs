﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SegExchLibrary
{
    public class APIResponse
    {
        public string Magic { get; set; }
        public string Password { get; set; }
        public Uri URL { get; set; }
    }
}
